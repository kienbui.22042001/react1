import React, { Component } from 'react';

class AddUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            tel: '',
            Permission: '',
        }
    }


    kiemTraTrangThai = () => {
        if(this.props.hienThiForm === true) {
            return (
                <div className="col">
                <form action="">
                <div className="card border-primary mb-3 mt-2">
                <div className="card-header">Thêm mới user vào hệ thống</div>
                <div className="card-body text-primary">
                    <div className="form-group">
                    <input
                                type="text"
                                name="name"
                                className="form-control"
                                placeholder="Tên user"
                                onChange  ={(event) => this.isChange(event)}
                                />
                    </div>
                    <div className="form-group">
                    <input type="text" name="tel" className="form-control" placeholder="Tel" onChange  ={(event) => this.isChange(event)} />
                    </div>
                    <div className="form-group">
                    <select name="Permission" className="custom-select" onChange  ={(event) => this.isChange(event)}>
                        <option value>Chọn quyền mặc định</option>
                        <option value={1}>Admin</option>
                        <option value={2}>Moderator</option>
                        <option value={3}>Normal</option>
                    </select>
                    </div>
                    <div className="form-group">
                    <input type="reset" value={"Thêm mới"} className="btn btn-block btn-primary" onClick = {(name, tel, Permission) => this.props.add(this.state.name,this.state.tel,this.state.Permission)}/>
                    </div>
                </div>
                </div>
                </form>
                </div>
            )
        }
    }

    isChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        this.setState({
            [name] : value
        });


    }

    render() {
        return (
            <div>
                {this.kiemTraTrangThai()}
            </div>

        );
    }
}

export default AddUser;