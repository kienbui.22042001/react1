import React, { Component } from 'react';
import EditUser from './EditUser';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            temValue: '',
            userObj: {}
        }
    }

    getUserEditInfo = (info) => {
        this.setState({
            userObj: info
        });
        this.props.getUserEditInfoApp(info);
    }

    hienThiNut = () => {
        if (this.props.hienThiForm === true  ){
            return <div className="btn btn-outline-secondary btn-block" onClick={() => this.props.ketNoi()}>Đóng</div>
        }else{
            return <div className="btn btn-outline-info btn-block" onClick={() => this.props.ketNoi( )}>Thêm mới</div>
        }
    }

    isChange = (event) => {
        this.setState({
            temValue: event.target.value
        });
        this.props.checkConnectProps(this.state.temValue)
    }

    isShowEditForm = () => {
        if(this.props.editUserStatus === true) {
            return <EditUser
            getUserEditInfo = {(info) => {this.getUserEditInfo(info)}}
            userEditObject = {this.props.userEditObject}
            changeEditUserStatus = {() => this.props.changeEditUserStatus()} />
        }
    }

    render() {

        return (
            <div className="col-12">
                <div className="row">
                    {this.isShowEditForm()}
                </div>
                <div className="form-group">
                    <div className="btn-group">
                        <input type="text"
                                    className="form-control"
                                    placeholder="Nhập từ khóa"
                                    style={{width: '500px'}}
                                    onChange={(event) => this.isChange(event)} />
                        <div className="btn btn-info" onClick = {(dl) => this.props.checkConnectProps(this.state.temValue)}>Tìm</div>
                    </div>
                    <div className="btn-group1 mt-3">
                        {this.hienThiNut()}
                    </div>
                </div>
                <hr/>
          </div>
        );
    }
}

export default Search;