import React, { Component } from 'react';
import TableDataRow from './TableDataRow';

class TableData extends Component {

    deleteClickButton = (idUser) => {
        this.props.deleteUser(idUser)
    }

    mappingDataUser = () => this.props.dataUserProps.map((value, key) => (
        <TableDataRow stt={key} key={key}
                                    id = {value.id}
                                   userName={value.name}
                                   tel = {value.tel}
                                   permission = {value.Permission}
                                   editFunClick = {(user) => this.props.editFun(value)}
                                   changeEditUserStatus = {() => this.props.changeEditUserStatus()}
                                   deleteClickButton = {(idUser) => this.deleteClickButton(idUser)}/>
    ))

    render() {
        return (
            <div className="col">
            <table className="table table-striped table-hover ">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên</th>
                    <th>Điện thoại</th>
                    <th>Quyền</th>
                    <th>Thao tác</th>
                </tr>
                </thead>
                <tbody>
                    {this.mappingDataUser()}
                </tbody>
            </table>
            </div>
        );
    }
}

export default TableData;