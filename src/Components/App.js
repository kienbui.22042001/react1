import React, { Component } from 'react';
import AddUser from './AddUser';
import Header from './Header';
import Search from './Search';
import TableData from './TableData';
import DataUser from './Data.json'

const { v4: uuidv4 } = require('uuid');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hienThiForm: false,
      data: [],
      searchText: '',
      editUserStatus: false,
      userEditObject: {}
    }
  }


  componentWillMount() {
    if(localStorage.getItem('userData') === null) {
      localStorage.setItem('userData', JSON.stringify(DataUser));
    }else{
      var temp = JSON.parse(localStorage.getItem('userData'));
      this.setState({
        data: temp
      });
    }
  }


  doiTrangThai = () => {
    this.setState({
      hienThiForm: !this.state.hienThiForm
    });
  }

  getTextSearch = (dl) => {
    this.setState({
      searchText: dl
    });
    console.log("Dữ liệu nhận được" +  this.state.searchText);
  }

  getNewUserData = (name, tel, Permission) => {

    var item = {};
    item.id =  uuidv4();;
    item.name = name;
    item.tel = tel;
    item.Permission = Permission;

    var items = this.state.data;
    items.push(item);

    this.setState({
      data: items
    });

    localStorage.setItem('userData', JSON.stringify(items));

  }

  editUser = (user) => {
    console.log("Kết nối thành công roi");
    this.setState({
      userEditObject: user
    });
    console.log(user);
  }

  changeEditUserStatus  = () => {
    this.setState({
      editUserStatus: !this.state.editUserStatus
    });
  }

  getUserEditInfoApp = (info) => {
    console.log("Du lieu da sua" + info.name);
    this.state.data.forEach((value, key) => {
      if (value.id === info.id) {
        value.name = info.name;
        value.tel = info.tel;
        value.Permission = info.Permission;
      }
    })
    localStorage.setItem('userData', JSON.stringify(this.state.data));

  }

  deleteUser  = (idUser) =>  {
    var tempData = this.state.data.filter(item => item.id !== idUser);
    this.setState({
      data: tempData
    });
    localStorage.setItem('userData', JSON.stringify(tempData));
  }

  render() {

    localStorage.setItem('key1','hello')
    console.log(localStorage.getItem('key1'));
    var ketQua = [];
    this.state.data.forEach((item) => {
      if(item.name.indexOf(this.state.searchText) !== -1) {
        ketQua.push(item);
      }
    })
    return (
      <div>
        <Header/>
        <div className="searchForm mb-3">
          <div className="container">
            <div className="row">
              <Search  ketNoi = {() => this.doiTrangThai()}
                             hienThiForm = {this.state.hienThiForm}
                             checkConnectProps = {(dl) => this.getTextSearch(dl)}
                             editUserStatus = {this.state.editUserStatus}
                             changeEditUserStatus = {() => this.changeEditUserStatus()}
                             userEditObject = {this.state.userEditObject}
                             getUserEditInfoApp = {(info) => this.getUserEditInfoApp(info)} />
              <TableData dataUserProps = {ketQua}
                                  editFun ={(user) => this.editUser(user)}
                                  changeEditUserStatus = {() => this.changeEditUserStatus()}
                                  deleteUser = {(idUser) => this.deleteUser(idUser)}/>
              <AddUser hienThiForm = {this.state.hienThiForm}
                               add = {(name, tel,  Permission) => this.getNewUserData(name, tel,  Permission)} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;