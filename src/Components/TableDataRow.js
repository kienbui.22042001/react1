import React, { Component } from 'react';

class TableDataRow extends Component {

    permissionShown = () => {
        if (this.props.permission === 1) { return "Admin ";}
        else if (this.props.permission === 2) { return "Moderate ";}
        else{ return "Normal User ";}

    }

    editClick = () => {
        this.props.editFunClick();
        this.props.changeEditUserStatus();
    }

    deleteClickButton = (idUser) => {
        this.props.deleteClickButton(idUser)
    }

    render() {
        return (
                 <tr>
                    <td >{this.props.stt+1}</td>
                    <td>{this.props.userName}</td>
                    <td>{this.props.tel}</td>
                    <td>{this.permissionShown()}</td>
                    <td>
                    <div className="btn-group">
                        <div className="btn btn-warning sua" onClick={() => this.editClick()}>
                        <i className="fa fa-edit" /> Sửa
                        </div>
                        <div
                            onClick={(idUser) => this.deleteClickButton(this.props.id)}
                        className="btn btn-danger sua">
                        <i className="fa fa-delete" /> Xóa
                        </div>
                    </div>
                    </td>
                </tr>
        );
    }
}

export default TableDataRow;